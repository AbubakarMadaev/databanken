-- Werken met samengestelde logische operatoren
-- Oefening 2 
use ModernWays;
select Familienaam, Titel 
from Boeken
   where Familienaam = 'Beth' or Familienaam = 'Bly';
   
-- Oefening 3
use ModernWays;
select Familienaam, Titel 
from Boeken
   where (not Familienaam = 'Beth');
   
-- Oefening 4
use ModernWays;
insert into Boeken (Voornaam, Familienaam, Titel, Stad, Uitgeverij, Verschijningsjaar, Categorie, InsertedBy)
 values ('Hugo', 'Claus', 'De verwondering', 'Antwerpen', 'Manteau', '1970', ' ', 'AM');
 
-- Oefening 5
insert into Boeken (Voornaam, Familienaam, Titel, Stad, Uitgeverij, Verschijningsjaar, Categorie, InsertedBy)
 values ('Hugo', 'Raes', 'Jagen en gejaagd worden', 'Antwerpen', 'De Bezige Bij', '1954', ' ', 'AM');
 
-- Oefening 6
insert into Boeken (Voornaam, Familienaam, Titel, Stad, Uitgeverij, Verschijningsjaar, Categorie, InsertedBy)
 values ('Jean-Paul', 'Sarthe', 'Het zijn en het niets', 'Parijs', 'Gallimard', '1943', ' ', 'AM');
 
-- Oefening 7
select Voornaam, Familienaam, Titel from Boeken
where Voornaam = 'Hugo' or Voornaam = 'Jean-Paul'; 

-- Oefening 8
select Voornaam, Familienaam, Titel, Verschijningsjaar from Boeken
where 
   (Voornaam = 'Hugo' or Voornaam = 'Jean-Paul') 
   and (Verschijningsjaar = '1970'); 

-- Oefening 9
update Boeken
   set Familienaam = 'Sartre'
   where Familienaam = 'Sarthe';
   
 -- Oefening 10
update Boeken
   set Categorie = 'Literatuur'
   where (Familienaam = 'Claus' and Voornaam = 'Hugo') or
      (Familienaam = 'Raes' and Voornaam = 'Hugo');
      
-- Oefening 11
update Boeken
   set Categorie = 'Filosofie'
   where (Familienaam = 'Sartre' and Voornaam = 'Jean-Paul' and Titel = 'Het zijn en het niets');
   
 -- Oefening 12
 select Voornaam, Familienaam, Titel from Boeken
where (Familienaam = 'Sartre' and Categorie = 'Filosofie') or (Voornaam = 'Hugo' and Familienaam = 'Claus');