-- AM
-- 18 april 2018
-- Kolom Id toevoegen en primary key van maken 
--
USE ModernWays;
ALTER TABLE Boeken ADD Id INT AUTO_INCREMENT,
    ADD CONSTRAINT pk_Boeken_Id PRIMARY KEY (Id);