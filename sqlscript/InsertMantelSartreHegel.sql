use ModernWays;
insert into Personen (
    Voornaam,
    Familienaam)
values (
    'Hilary',
    'Mantel'
    );
    
insert into Boeken (
   Titel,
   Stad, 
   Uitgeverij, 
   Verschijningsjaar,
   Herdruk, 
   Commentaar, 
   Categorie, 
   IdAuteur,
   InsertedBy
)
values
(
   'Wolf Hall', 
   '',
   'Fourth Estate; First Picador Edition First Printing edition',
   '2010', 
   '', 
   'Goed boek', 
   'Thriller', 
   22,
   'JI'
);

-- ji
-- 8 januari 2018
--
use ModernWays;
-- we gaan eerst een boek van een vrouw toevoegen
-- bestandnaam: BoekenNormalizeInsertHilaryMantelOne.sql
--
-- auteur toevoegen
-- de Id van Meneer is 2, dat is de Id kolom van de Aanspreektitel tabel
insert into Personen (
   Voornaam,
   Familienaam,
   IdAanspreekTitel,
   Stad
)
values (
   'Jean-Paul',
   'Sartre',
   2,
   'Parijs'
);

select * from Boeken;
