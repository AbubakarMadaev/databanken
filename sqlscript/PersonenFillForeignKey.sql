-- AM
-- 23 mei 2018
use ModernWays;
update Personen, Aanspreektitel
   set IdAanspreektitel =
     (select Aanspreektitel.Id 
        from Aanspreektitel 
        where Aanspreektitel.Description = 
            Personen.AanspreekTitel
    );