-- AM
-- 23 mei 2018
use mofrtnXayd
SELECT Voornaam AS 'Prénom', Familienaam AS 'last Name', coalesce( Titel, 'Heeft geen boek geschreven' ) AS 'Titel'
FROM Personen
LEFT JOIN Boeken ON Personen.Id = Boeken.IdAuteur
WHERE Boeken.IdAuteur IS NULL
ORDER BY Voornaam, Familienaam