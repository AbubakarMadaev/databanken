create table Personen (
    Id int auto_increment,
    Voornaam nvarchar(255) not null,
    Familienaam nvarchar(255) not null, 
    Leeftijd int,
    constraint pk_Personen_Id primary key(Id)
    );