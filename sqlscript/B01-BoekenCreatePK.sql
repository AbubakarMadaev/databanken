-- AM
-- 18 april 2018
--
-- Begin altijd met op te geven in welke database
-- je wilt werken!!!!!!
use ModernWays;
-- ga na als de tabel al bestaat
drop table if exists Boeken;

-- de naam van de tabel in pascalnotatie
create table Boeken(
	Voornaam nvarchar(50), 
	Familienaam nvarchar(80),
	Titel nvarchar(255),
    Uitgeverij nvarchar(255),
	Stad nvarchar(50),
	-- alleen het jaartal, geen datetime 
	-- omdat de kleinste datum daarin 1753 is
	-- varchar omdat we ook jaartallen kleiner dan 1000 hebben
	Verschijningsjaar varchar(4),
	Herdruk varchar(4),
	Commentaar nvarchar(2000),
    Categorie nvarchar(120),
    InsertedBy nvarchar(255),
	Id int auto_increment not null,
	constraint pk_Boeken_Id primary key(id)
);
