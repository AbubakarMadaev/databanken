-- AM
-- 14 maart 2018
-- Bestandsnaam: BoekenInsertAllJI.sql
-- gegevens komen uit de Korte Introductie
--
-- Opmerking
-- Als je een enkel aanhalingsteken in een string wil typen in SQL
-- moet je het enkel aanhalingstelen escapen omdat het enkel aanhalingsteken
-- eigenlijk een instructie is die aangeeft dat wat volgt een tekenreeks is.
-- Om een enkel aanhalingsteken te escapen ontdubbel je het bv:
-- 'Le siècle d''or'
-- Het eerste enkel aanhalingsteken begint de string, het tweede en derde geven
-- een enkel aanhalingsteken dat in de string wordt opgenomen en het vierde
-- sluit de string af.
-- Zet de use in commentaar om te voorkomen dat medecursisten gegevens
-- in jou tabel inserten. We gaan immers die insert scipts met elkaar delen.
use ModernWays;
insert into Boeken (
   Voornaam,
   Familienaam,
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsjaar,
   Herdruk,
   Commentaar,
   Categorie,
   InsertedBy
)
values 
('Samuel', 'Ijsseling', 'Heidegger. Denken en Zijn. Geven en Danken', 'Amsterdagm', '', '2014', '', 'Nog te lezen', 'Filosofie', 'JI'),
('Jacob', 'Van Sluis', 'Lees wijzer bij Zijn en Tijd', '', 'Budel', '1998', '', 'Goed boek', 'Filosofie', 'JI'),
('Emile', 'Benveniste', 'Le vocabulaire des institutions Indo-Européennes. 2. Pouvoir droit religion', 'Paris?', 'Les ditions de minuit', '1969', '', 'Een goed geschiedenis boek','Linguistiek', 'JI'),
('Evert W.', 'Beth', 'De Wijsbegeerte der Wiskunde. Van Parmenides tot Bolzano', 'Antwerpen', 'Philosophische Biliotheek Uitgeversmij. N.V. Standaard-Boekhandel', '1944', '?', 'Een goed boek', 'Filosofie', 'JI'),
('Evert W.', 'Beth', 'Wijsbegeerte der Wiskunde', 'Antwerpen', 'Philosophische Biliotheek Uitgeversmij. N.V. Standaard-Boekhandel', '1948', '?', 'Een goed boek','Wiskunde', 'JI'),
('Rémy', 'Bernard', 'Antonin le Pieux. Le siècle d''or de Rome 138-161', '?', 'Librairie Arthme Fayard', '2005', '?', 'Een goed boek', 'Geschiedenis', 'JI'),
('Marc', 'Bloch', 'Rois et serfs et autres écrits sur le servage', 'Paris', 'La boutique de l''histoire', '1996', '?', 'Een goed boek','Geschiedenis', 'JI'),
('Pierre', 'Bonte en Michel Izard', 'Dictionnaire de l''etnologie et de l''anthropologie', '?', 'PUF', '1991', '?', 'Een goed boek','Anthropologie', 'JI'),
('Robert', 'Bly', 'The sibling society', 'Londen', 'Persus', '1996', '?', 'Een interessant boek', 'Antropologie','JI'),
('Fernand', 'Braudel', 'De middellandse zee. Het landschap en de mens', 'Amsterdam/Antwerpen', 'Uitgeverij Contanct', '1992', '?', 'Uit het Frans vertaald: La méditerranée. La part du milieu. Parijs: Librairie Armand Colin, 1966', 'Geschiedenis', 'JI'),
('Timothy', 'Gowers', 'Wiskunde, de kortste introductie', 'Utrecht', 'Uitgeverij Het Spectrum B.V.', '2003', '', 'Oorpronkelijke titel: Mathematics a very schort introduction. Oxford University Press, 2002', 'Wiskunde', 'JI');