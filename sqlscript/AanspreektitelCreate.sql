use ModernWays;
drop table if exists Aanspreektitel;
create table Aanspreektitel (
    Id int auto_increment,
    Description nvarchar(50),
    constraint pk_Aanspreektitel_Id primary key(Id)
    )