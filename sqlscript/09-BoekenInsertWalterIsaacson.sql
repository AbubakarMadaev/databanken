-- AM
-- 28 februari 2018
-- Werken met INSERT
-- Bestandsnaam:BoekenInsertWalterIsaacson.sql
use ModernWays;
insert into Boeken(
   Voornaam,
   Familienaam,
   Titel,
   Uitgeverij,
   Verschijningsjaar,
   Commentaar,
   Categorie,
   InsertedBy
)
values (
   'Walter',
   'Isaacson',
   'Steve Jobs-de biografie',
   'Spectrum',
   '2015',
   'Nog te lezen',
   'Biografie',
   'Abubakar Madaev'
);