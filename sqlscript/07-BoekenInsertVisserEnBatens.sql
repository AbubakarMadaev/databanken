-- AM
-- 28/02/2018
-- twee rijen in een keer toevoegen
-- BoekenInsertVisserEnBatens.sql
use ModernWays;
insert into Boeken (
   Voornaam,
   Familienaam,
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsjaar,
   Herdruk,
   Commentaar,
   Categorie,
   InsertedBy
)
values
('Gerard', 'Visser', 'Heideggers vraag naar de techniek', 'Nijwegen', '', '2014', '', '', '', ''),
('Diderik', 'Batens', 'Logicaboek', '', 'Garant', '1999', '', '', '', '')