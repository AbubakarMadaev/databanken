use ModernWays;
select 
   Aanspreektitel.Description,
   Voornaam, 
   Familienaam, 
   Titel, 
   Verschijningsjaar, 
   InsertedBy
from Boeken 
inner join Personen on Boeken.IdAuteur = Personen.Id
inner join Aanspreektitel on Personen.IdAanspreektitel = Aanspreektitel.Id
where (Voornaam = 'Hilary' and Familienaam = 'Mantel')
       or (Voornaam = 'David' and Familienaam = 'Hume')
       
       select 
   Aanspreektitel.Description,
   Voornaam, 
   Familienaam, 
   Titel, 
   Verschijningsjaar, 
   InsertedBy
from Boeken 
inner join Personen on Boeken.IdAuteur = Personen.Id
inner join Aanspreektitel on Personen.IdAanspreektitel = Aanspreektitel.Id
where (Voornaam = 'Hilary' and Familienaam = 'Mantel') and
	(Verschijningsjaar between 2005 and 2007)
